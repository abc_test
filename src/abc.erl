-module(abc).
-compile(export_all).
-include_lib("stdlib/include/qlc.hrl").

my_q(T)->
    Q = qlc:q([X || X <- mnesia:table(T)]),
    F = fun()->
		 qlc:e(Q)
	end,
    {atomic, Val} = mnesia:transaction(F),
    Val.

my_insert(R)->
    F = fun()->
		mnesia:write(R)
	end,
    mnesia:transaction(F).

my_read(T, K)->
    F = fun()->
		mnesia:read({T, K})
        end,
    mnesia:transaction(F).


f()->1.
